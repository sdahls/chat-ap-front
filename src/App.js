import React from 'react';
import logo from './logo.svg';
import './App.css';
import ChattThing from "./component/ChattThing"
import {BrowserRouter, Route} from 'react-router-dom'
import ChattRoom from './component/ChattRoom'



function App() {
  return (
    <BrowserRouter>
    
      <Route exact path="/" component={ChattThing}/>
      <Route  path="/chattroom" component={ChattRoom}/>

      
     
    </BrowserRouter>
  );
}

export default App;
