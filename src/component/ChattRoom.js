import React, { Component } from 'react'
import Chattframe from './ChattFrame'
import io from 'socket.io-client'


class ChattRoom extends Component{

    
    constructor(props){
        super(props)

        this.state={
            messages: [],
            localUsers:[],
            currentMessage:"",
            currentUser:"",
            socket: io('localhost:8080', {
                transportOptions:{
                polling:{
                    extraHeaders:{
                       Authorization: this.props.location.state.token.token
                    }
                    
                }
            }
            }),
            token:""
    }

        
    // this.state.socket.on('message', data =>{
    //     let msgText = data.user + ": " + data.message
    //     this.setState({messages: [...this.state.messages, msgText]})
    // })
   
    }


async componentDidMount(){
    
    console.log(this.props.location.state.token.token)

    //console.log("statelocation: " + this.props.location.state)
    this.setState({
        currentUser: this.props.location.state.currUse,
        token: this.props.location.state.token
    })

    this.state.socket.emit('user', this.state.currentUser)

    this.state.socket.on('user', users=>{
        this.setState({
            localUsers: [...this.state.localUsers, ...users]
        })
         console.log(this.state.localUsers.length)
    })

   
    
    this.state.socket.on('message', msg=>{
        if(!msg.msg==""){
      this.setState({
          messages: [...this.state.messages, msg]
      })  
      }
    })
    
}

onMessageChanged = ev => {
    this.setState({
        currentMessage: ev.target.value
    })
}

onSendClick = () => {
    this.state.socket.emit('chatmessage', this.state.currentMessage, this.state.currentUser)
}


render(){

    return(

        <div>
            {<h1>Welcome to the chatt {this.state.currentUser}. its buttugly...</h1> /* 
    <div>thera are currently: {this.state.localUsers.length} users</div> */}
            <div>
                { 
                    this.state.messages.map(msg => {
                        return (
                            <div>
                                <p className="meta">{msg.user}:</p> <p className="text">{msg.msg}</p>
                            </div>
                        )
                    })
                }

            </div>

            <div>
            <form>
            <input type="text" placeholder="input chattmessage" onChange={ this.onMessageChanged } />
            <button type="button" onClick={this.onSendClick}>Send Message</button>
            <p></p>
            </form>
            </div>
        </div>
    ) 
}


}
export default ChattRoom