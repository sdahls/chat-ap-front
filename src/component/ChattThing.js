import React, { Component } from 'react'
import Axios from 'axios'

class ChatThing extends Component {



    state = {
        currentUser: "",
        messageObjects: { user: "", Message: "" },
        messages: []
    }

    onRegisterClick = () => {

        
        Axios.post('http://localhost:8080/createtoken', {
            username: this.state.currentUser
        }).then((response) => {

            let passOb = {
                currUse: this.state.currentUser,
                token: response.data
            }

            return passOb
        }).then((passOb) => {
            this.props.history.push("/chattroom", passOb)
        }).catch(function (error) {
            console.log(error)
        })

        
    }

    setUser = ev => {
        console.log(ev)
    }

    onUsernameChanged = ev => {
        this.setState({
            currentUser: ev.target.value
        })
    }


    render() {

        console.log(this.state);
        return <div>
            <form>
                <input type="text" placeholder="Enter a username" onChange={this.onUsernameChanged} />
                <button type="button" onClick={this.onRegisterClick}>Join the chat</button>
            </form>
            <p></p>
        </div>
    }



}

export default ChatThing;